/**
 * Run the app, gulp -> (dev-version) and gulp build -> (dist-version)
 */

var gulp             = require('gulp');
var pug              = require('gulp-pug');
var sass             = require('gulp-sass');
var prefix           = require('gulp-autoprefixer');
var useref           = require('gulp-useref');
var uglify           = require('gulp-uglify');
var cssnano          = require('gulp-cssnano');
var minifyhtml       = require('gulp-minify-html');
var imagemin         = require('gulp-imagemin');
var cache            = require('gulp-cache');
var gulpIf           = require('gulp-if');
var del              = require('del');
var sourcemap        = require('gulp-sourcemaps');
var changed          = require('gulp-changed');
var runSequence      = require('run-sequence');
var browserSync      = require('browser-sync').create();
var browserSyncBuild = require('browser-sync').create();

var messages = {
  reload: "<span color='green'>Reloading</span>",
  sassError: "<span color='red'>Error in sass"
}

function handleError(err) {
  console.log(err.toString());
  this.emit('end');
}

/**
 * BrowseSync dev and dist
 */

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'app'
    }
  })
})

gulp.task('browserSyncBuild', function() {
  browserSync.init({
    server: {
      baseDir: 'dist'
    }
  })
})



/**
 * Compile sass and pug.
 * Sourcemap
 * Autoprefixer
 */

gulp.task('sass', function() {

  return gulp.src('app/assets/css/main.scss')

  .pipe(changed('app/assets/css', {
    extension: '.'
  }))

  .pipe(sourcemap.init())
  .pipe(sass({
    includePaths: ['css'],
  }))
  .on('error', handleError)


  .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {
    cascade: false
  }))

  .pipe(sourcemap.write('/maps'))
  .pipe(browserSync.reload({
    stream: true
  }))
  .pipe(gulp.dest('app/assets/css'))
})

gulp.task('pug', function() {

  return gulp.src('app/_pugfiles/index.pug')
  .pipe(pug({
    pretty: "\t"
  }))
  .on('error', handleError)

  .pipe(gulp.dest('app/'))
})

/**
 * Watch task
 */

gulp.task('watch', function() {

  gulp.watch('app/assets/css/**/*', ['sass'])
  gulp.watch('app/_pugfiles/**/*.pug', ['pug'])
  gulp.watch('app/*.html', browserSync.reload)
  gulp.watch('app/assets/js/**/*.js', browserSync.reload)
})

/**
 * Optimize JS, CSS, HTML and Images
 */

gulp.task('useref', function() {

  return gulp.src('app/*.html', {
    base: 'app'
  })

  .pipe(useref())
  .pipe(gulpIf('*.js', uglify()))
  .pipe(gulpIf('*.css', cssnano()))
  .pipe(minifyhtml())
  .pipe(gulp.dest('dist'))
})

gulp.task('images', function() {

  return gulp.src('app/assets/img/**/*.+(png|jpg|jpeg|gif|svg)')
  .pipe(cache(imagemin({
    interlaced: true
  })))

  .pipe(gulp.dest('dist/assets/img'))
})

/**
 * Copy fonts/
 */

gulp.task('fonts', function() {
  return gulp.src('app/assets/fonts/**/*')
  .pipe(gulp.dest('dist/assets/fonts'))
})

/**
 * Clean the dist version and cache
 */

gulp.task('clean:dist', function() {
  return del.sync(['dist/**', '!dist', '!dist/assets/img/**'])
})

gulp.task('clean', function(cb) {
  return cache.clearAll(cb);
})

/**
 * Run the app default(dev) and build(dist)
 */

gulp.task('default', function(callback) {
  runSequence(
    'pug',
    ['sass', 'browserSync', 'watch'],
    callback
  )
})

gulp.task('build', function(callback) {
  runSequence(
    'clean:dist',
    'sass',
    'fonts',
    'useref',
    ['images'],
    'browserSyncBuild',
    callback
  )
})
