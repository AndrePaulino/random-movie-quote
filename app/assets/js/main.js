(function($) {
  var currentQuote,
      currentAuthor,
      $selectors,
      $quoteText,
      $quoteAuthor,
      $newQuote,
      $tweetQuote,
      tweetUrl;

  function getQuote() {
    $.ajax({
      headers: {
        "X-Mashape-Key": 'ThCpUkfovlmshSMsKNhK4boRCyAqp1Fx2rLjsnHopLbKPnfprc',
        Accept: 'application/json',
        "Content-Type": 'application/x-www-form-urlencoded'
      },
      url: 'https://andruxnet-random-famous-quotes.p.mashape.com/?cat=movies',
      dataType: 'json',
      success: function(data) {
        currentQuote = data.quote;
        currentAuthor = data.author;
        tweetAddress();
      }
    });
  }

  function newQuoteClick()  {
    $newQuote.on('click', function() {
      getQuote();
      $selectors.fadeOut(500, function() {
        $quoteText.html(currentQuote);
        $quoteAuthor.html('- '+ currentAuthor);
      });
      $selectors.fadeIn(500);
    });

  }

  function tweetAddress() {
    tweetUrl = "https://twitter.com/intent/tweet?hashtags=quotes&text=" + '"' + currentQuote + '" ' + currentAuthor;
    tweetUrl = tweetUrl.replace(/\s+/g, "%20");
  }

  function tweetQuote() {
    $tweetQuote.on('click', function() {
      window.open(tweetUrl);
    });
  }


  function bindings() {
    tweetAddress();
    newQuoteClick();
    tweetQuote();
  }

  $(document).ready(function() {
    currentQuote = "You talking to me?";
    currentAuthor = "Taxi Driver";
    $quoteText = $('.quote-text');
    $quoteAuthor = $('.quote-author');
    $selectors = $('.quote-text, .quote-author');
    $newQuote = $('.new-quote');
    $tweetQuote = $('.twitter-share-button');
    tweetUrl = "";

    bindings();
  });
})(jQuery);
